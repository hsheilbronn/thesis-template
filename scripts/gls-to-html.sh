#!/usr/bin/env bash

set -eu -o pipefail

cat chapter/98_glossary.tex |
    grep "^.new" |
    grep -v "This is a test" |
    grep -v "asdf" |
    sed 's/.new[a-z]*{\(.*\)}{\(.*\)}{\(.*\)}/<p data-sort="\2"><a id="glo:\1"><\/a><strong>\2<\/strong>: \3<\/p>/'
