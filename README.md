# Master Thesis

This is the repository containing the documentation for
my master thesis in Software Engineering and Management
at Heilbronn University by Martin Sigloch in WS2021/22.

## Generate PDF

There are several build tasks defined in the `makefile`.
Some of these are executed on every commit by Gitlab-CI.
It starts a docker container containing the dependencies
to build (pandoc, mermaid, plantuml and pandoc-plugins).
Instead of using these plugins you can run the following

    PATH=$PATH:$PWD/scripts make pandoc

which will skip diagram generation and instead shows the
code blocks unchanged, but requires `make` and `pandoc`.

The pdf can be found at gitlab (Password: YourPassword):

https://hsheilbronn.gitlab.io/thesis-template/thesis.pdf

## Files and folders

The following files and folders are most relevant if you
want using the project when for writing your own thesis:

- chapter: This contains all the content of your thesis.
- csl: This folder contains some citation styles. If you
       want others, you can get them at the zotero style
       repo at https://www.zotero.org/styles then put it
       into this folder and change the `--cls` attribute
       inside the makefile to match the style file name.
- figures: This folder is for static figures to include.
- settings: This folder contains templates for pandoc to
            use when generating PDFs and other settings.
- .gitlab-ci.yml: Specifies tasks to run in GitLab CI on
                  every commit or tagged commits. If the
                  task succeeds, you can download all of
                  the generated documents an also access
                  a password protected pdf in the gitlab
                  pages. The makefile stores a password.
- makefile: Specifies all commands to generate documents
            in HTML/PDF formats, and setup dependencies.
- papers.bib: Bibliography collections in BibTex format.
