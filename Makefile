.PHONY: version
FORMAT ?= pdf

all: pandoc

pandoc: thesis

install:
	sudo apk add --no-cache qpdf
	sudo tlmgr update --self
	sudo tlmgr install \
		bookman \
		tocbibind \
		lipsum \
		quotchap \
		threeparttable \
		glossaries mfirstuc xfor tracklang datatool \
		bigfoot xstring

preprocess:
	mkdir -p output/figures
	scripts/code-caption

thesis-tex:
	FORMAT=tex make thesis

thesis: version preprocess
	MERMAID_FILTER_LOC="${PWD}/output/figures" \
	MERMAID_FILTER_WIDTH="2000" \
		pandoc \
		-f markdown+inline_code_attributes \
		-H settings/preamble.tex \
		-H chapter/80_glossary.tex \
		--listings \
		--resource-path=./chapter/ \
		--template settings/template.tex \
		--top-level-division=chapter \
		--bibliography=papers.bib \
		--csl="csl/ieee.csl" \
		--citeproc \
		-F mermaid-filter \
		-F pandoc-plantuml \
		-F pandoc-include \
		output/combined.md \
		$(shell find chapter/ -type f -name '9*.tex' | sort | sed 's/^/-A /') \
		-V papersize:a4 \
		-o "./output/thesis.${FORMAT}"
# TODO?: support short image caption: https://github.com/martisak/pandoc-shortcaption
# TODO?: export plantuml images to output/figures/

thesis-nixos:
	nix-shell -p \
		python310Packages.pygments python39Packages.pandocfilters \
		pandoc-plantuml-filter plantuml python39Packages.pygments librsvg perl \
		--command 'PATH=$$PATH:./scripts make thesis'

thesis-nixos-tex:
	nix-shell -p \
		python310Packages.pygments python39Packages.pandocfilters \
		pandoc-plantuml-filter plantuml python39Packages.pygments librsvg perl \
		--command 'PATH=$$PATH:./scripts make thesis-tex'

# requires while-change script from https://gitlab.com/f0i/bin
watch-nixos:
	while-change -w chapter/ -w settings/ -r make thesis-nixos

CI_COMMIT_TIMESTAMP ?= $(shell date --iso-8601=s)
CI_COMMIT_SHORT_SHA ?= untracked
CI_COMMIT_REF_NAME ?= local

# First line: get timestamp, hash and branch/tag name
# Second line: remove timezone from timestamp
# Third line: remove HH:MM:SS from timestamp
# Fourth line: write to `./version`
version:
	echo "${CI_COMMIT_TIMESTAMP}: ${CI_COMMIT_SHORT_SHA} (${CI_COMMIT_REF_NAME})" \
		| sed 's/+[01][0-9]\:[03]0//' \
		| sed 's/T..\:..\:..//' \
		> version

ci:
	make thesis-tex
	make thesis

pages: pandoc
	mkdir -p public
	# Add password for public pdf
	qpdf --encrypt 'YourPassword' 'YourPassword' 256 -- output/thesis.pdf public/thesis.pdf
	# If you don't want a password use this instead:
	#cp output/thesis.pdf public/thesis.pdf

