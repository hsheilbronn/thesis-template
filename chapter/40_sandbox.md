\ifdebug

# Sandbox

This chapter contains examples of figures, references, tables, etc.

If you remove the debug and todo option from `00_meta.md`, this chapter won't show up any more. (because of the `\ifdebug` statment).
This will also set the link color to black!
**TODO!: remove `debug: true` and `todo: true` from `00_meta.md` before generating the final document!**

\todo{disable debug and todo in meta.md}

## Figure

See figure \ref{hhn-logo} for an example.

![Heilbronn University Logo \label{hhn-logo}](../hhn_logo.png)

Figures will be placed with the latex settings `\def\fps@figure{htb}`, defined in `template.tex`.
This could cause placement inside source code listings.

Two images side by side with a caption: <https://stackoverflow.com/a/51490189>

### Mermaid

Figure \ref{sandbox-mermaid} shows a mermaid diagram.

```mermaid #sandbox-mermaid "Sample Mermaid diagram"
flowchart TD
    a[Node A] --> b[Node B]
```

### PlantUML

Figure \ref{sandbox_plantuml} shows a PlantUML diagram.

```plantuml #sandbox_plantuml "Sample Plantuml diagram" width="10%"
A --> B
```

```plantuml
!$idle = "{-}"

concise "DieBox" as dieBox
concise "DerKom" as derKom
concise "DasTool" as dasTool


@dieBox
0 is send
dieBox -> derKom@+25 : GET
+25 is $idle
+75 is recv
+25 is $idle
+25 is send
dieBox -> derKom@+25 : GET\nIf-Modified-Since: 150
+25 is await
+50 is recv
+25 is $idle
@100 <-> @275 : no need to re-request from server

@derKom
0 is $idle
25 is recv
+25 is work
+25 is send
derKom -> dasTool@+25 : 200 OK\nExpires: 275
+25 is $idle
+75 is recv
+25 is send
derKom -> dieBox@+25 : 304 Not Modified
+25 is $idle

@dasTool
0 is $idle
100 is fresh
+175 is stale
```

```plantuml
' https://github.com/plantuml/plantuml-stdlib

!include <logos/rust>
!include <logos/linux-tux>
!include <logos/javascript>
!include <logos/C>
!include <logos/ruby>
!include <logos/elm>
!include <logos/json>

listsprites
```

## Table

Table \ref{sandbox_table} shows a table with caption.

| asdf | qwer |
| ---- | ---- |
| test | 1234 |
: A sample table with caption \label{sandbox_table}

## Source code

The code sample in listing \ref{sandbox_code} shows some source code.

```rust #sandbox_code "Example code"
fn main -> Option<u8> {
    panic!("asdf qwer");
    Some(42) // comment
}
```

## Comments

Comments can be added using the `\comment{This is a comment}` command.
\comment{This is a comment}
They show up as a side note.
To hide them, you can comment out the debug option in `00_meta.md`.

Longer blocks of text can be hidden the same way using `\inlinecomment`.
If `00_meta.md` contains `debug: true`, `\inlinecomment`s  will show as normal text.

## Placeholder text

Short: `\asdf`, medium: `\asdfasdf`, long: `\asdfasdfasdf`

This will also add `\todo{Add text}` to generate a note on the side.

\asdf

## Citation/References

Missing citation reference will generate a warning<!-- [@missing_citation]-->.

All citations will be added to `99_references.md` file.

## Acronyms/Glossary

Acronyms entries are defined in `98_glossary.tex`

- First call of `\gls{asdf}`: \gls{asdf}
- Second call: \gls{asdf}

Glossaries entries also

- `\gls{test}`: \gls{test}
- `\glsdesc{test}`: \glsdesc{test}

Unused entries won't show up in the glossary even if they are defined in `98_glossary.tex`.

## Link to figures, chapters, etc

\label{custom-label-for-section}

Figures, Listings, Tables, Chapters, and Sections can be referenced by its number:

`Figure \ref{hhn_logo}`:
 Figure \ref{hhn_logo}

`has the description: \nameref{hhn_logo}`:
 has the description: \nameref{hhn_logo}

`Chapter \ref{sandbox} is \nameref{sandbox}`:
 Chapter \ref{sandbox} is \nameref{sandbox}

`Section \ref{custom-label-for-section} is \nameref{custom-label-for-section}`:
Section \ref{custom-label-for-section} is \nameref{custom-label-for-section}

`\hyperref[sandbox]{My custom text}`:
 \hyperref[sandbox]{My custom text}

## Known issues

\todo{check if any of these issues occurred}

Check that no text is flowing out of the end of the page after each table: <https://latex.org/forum/viewtopic.php?t=25244>
Add a `\pagebreak{}` if needed.

Placement of figures can be unfavorable sometimes.
Go through the document at the end and check if the images are in the right place.
If not move them around or use the above trick to force their position and set the caption manually.

Missing references don't generate warnings but will result in \ref{missing-reference-asdf} inside the target document.
Use the search function to find them.

Long chapter and section headings can cause overlap in the header.
Go through all pages and check if they overlap.

\fi
