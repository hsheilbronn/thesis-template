\begin{titlepage}

\thispagestyle{empty}

% Header with logo (change `hspace` and `width` if needed)
\begin{tabular}{rcl}
  \hspace{9cm}
  \includegraphics[width=5cm]{figures/Logo_HHN_EN.png}
\end{tabular}

%\rule{\textwidth}{0.4pt}

\vspace{2.5cm}
\begin{center}
\begin{spacing}{2}
  \textbf{\LARGE \thesistitle}
\end{spacing}
\end{center}

\vspace{1.5cm}
\begin{center}
  \textbf{\thesistype} \\

  \vspace{0.75cm}
  by \textbf{\thesisauthor} \\
  Matriculation Number: \studentid \\

  \vspace{1.5cm}
  
  Supervisors: \\
  \supervisor \\
  and \mentor \\

  \vspace{0.75cm}

  \department \\
  at \university \\

  \vspace{3cm}
  \tiny{Version: \input{version}} \\
  \ifdebug
    Mode: debug\iftodo{}, todo \fi
  \else
    \iftodo Mode: todo \fi
  \fi

\end{center}

\end{titlepage}

\newpage
\thispagestyle{empty}
\mbox{}
