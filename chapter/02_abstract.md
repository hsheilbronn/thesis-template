\frontmatter
\newcounter{roemisch}
\setcounter{roemisch}{\value{page}}

# Abstract

<!-- The broad setting: field of science, industry, ... -->
Most students have to write a thesis at the end of their studies.

<!-- Moving closer to your research question: what and why? -->
To get started, take a look at chapter \fullref{sandbox}

<!-- What problem did you set out to solve? -->
\asdf

<!-- How did you solve it, and what was the outcome? -->

<!-- These limits will be used to check further \gls{machining} processes and report an error to the machine control which can then initiate the appropriate reaction.-->

<!-- What immediately follows from the outcome? -->

<!-- Future outlook, and what may follow from your work -->

<!--This will enable easier and faster integration with other tools from the Linux ecosystem.-->

**Keywords:**: Markdown, LaTeX, GitLab CI, pandoc, make
