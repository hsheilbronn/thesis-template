\mainmatter

# Introduction

This document describes how to use the thesis build system.

## Context

The template was created while I was writing my masters thesis.

## Project goals

Hopefully this will be helpful to someone.
If it is, please let me know at thesis-template@projects.f0i.de

## Methodology

\asdf
