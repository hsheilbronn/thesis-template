---
title: Thesis Template for Writing a Master Thesis in Markdown 
thesistype: Master Thesis
author: Martin Sigloch
studentid: "171585"
supervisor: Alice the First
mentor: Bob the Second
description: Using pandoc, latex, make and custom scripts to generate pdf documents
department: "Department of Informatics (IT)"
university: "Heilbronn University of Applied Sciences"
location: "Heilbronn, Germany"
logo: Logo_HHN_EN.png
date: Friday, May 13, 2022

lang: en-US
papersize: a4
numbersections: true
link-citations: true
# enable comments and todos
debug: true
todo: true
---