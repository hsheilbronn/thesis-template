# Analysis

Based on the literature and market review, several requirements have been identified.

\asdf

## Use cases

\asdf

Figure \ref{usecases} gives an overview of the use cases for this topic.

```plantuml #usecases "Use cases" width="55%"
:Student: as s
left to right direction
rectangle " " {
s --> (Decide on topic)
s --> (Do some research)
s --> (Write a draft)
s --> (Rewrite everything)
}

:Professor: as p
left to right direction
rectangle " " {
p --> (Read draft)
p --> (Give feedback)
}

:User: as u
u <|-- s
u <|-- p
```

\asdfasdf
