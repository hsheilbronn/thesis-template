
\backmatter

\newpage
\pagenumbering{Roman}
\setcounter{page}{1}

\setcounter{secnumdepth}{-\maxdimen} <!-- remove section numbering -->
\addtocontents{toc}{\protect\setcounter{tocdepth}{1}} <!-- set to 0 to remove sections from TOC -->
